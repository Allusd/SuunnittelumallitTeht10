/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chainofresponsibility;

/**
 *
 * @author Aleks
 */
public class EsimiesTesti {
	
   private static Logger getLoggeriKetju(){

      Logger p = new Päällikkö(Logger.Päällikkö);
      Logger t = new Toimitusjohtaja(Logger.Toimitusjohtaja);
      Logger lh = new Lähiesimies(Logger.Lähiesimies);

      lh.setSeuraavaEsimies(p);
      p.setSeuraavaEsimies(t);

      return lh;	
   }

   public static void main(String[] args) {
      Logger loggeriKetju = getLoggeriKetju();

      loggeriKetju.loggeriViesti(Logger.Lähiesimies, 
         "2% korotus.");
       System.out.println("\n");
       
      loggeriKetju.loggeriViesti(Logger.Päällikkö, 
         "2-5 %korotus.");
       System.out.println("\n");
      
      loggeriKetju.loggeriViesti(Logger.Toimitusjohtaja, 
         "Yli 5% korotus.");
      System.out.println("\n");
   }
}
