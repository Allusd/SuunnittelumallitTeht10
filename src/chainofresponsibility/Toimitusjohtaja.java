/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chainofresponsibility;

/**
 *
 * @author Aleks
 */
public class Toimitusjohtaja extends Logger {

   public Toimitusjohtaja(int taso){
      this.taso = taso;
   }

   @Override
   protected void kirjoita(String viesti) {		
      System.out.println("Toimitusjohtaja: " + viesti);
   }
}
