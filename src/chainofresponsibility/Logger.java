/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chainofresponsibility;

/**
 *
 * @author Aleks
 */
public abstract class Logger {
   public static int Lähiesimies = 1;
   public static int Päällikkö = 2;
   public static int Toimitusjohtaja = 3;

   protected int taso;

   //seuraava esimies
   protected Logger seuraavaEsimies;

   public void setSeuraavaEsimies(Logger seuraavaEsimies){
      this.seuraavaEsimies = seuraavaEsimies;
   }

   
   // menee eteenpäin niin kauan, että se löytää esimiehen jolla on oikeudet
   //printtaa myös viestin niin kauan kunnes löytää oikean esimien
   public void loggeriViesti(int taso, String viesti){
      if(this.taso <= taso){
         kirjoita(viesti);
      }
      if(seuraavaEsimies !=null){
         seuraavaEsimies.loggeriViesti(taso, viesti);
      }
   }

   abstract protected void kirjoita(String message);
	
}
